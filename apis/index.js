// const {API_BASE_URL} = process.env
const API_BASE_URL = `${process.env.REACT_APP_API_BASE_URL || `https://api.dobzin.in`}/api/v1`;
export default API_BASE_URL;

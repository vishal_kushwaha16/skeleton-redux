import {combineReducers} from 'redux';
import undoable, {includeAction} from 'redux-undo';
import DesignBuilderReducer from './designBuilderReducer';
import DesignsReducer from './designsReducer';
import UserImageReducer from './userImageReducer';
import AuthReducer from './authReducer';
import LoadingReducer from './loadingReducer';
import FlickrReducer from './flickrReducer';
import selectedElement from './selectedElement';
import {UPDATE_ELEMENT} from '../actionTypes';
import userShapeList from './userShapeList';
import TemplateReducer from './templateReducer';
import MessageReducer from './messageReducer';

export default combineReducers({
  designBuilder: undoable(
    DesignBuilderReducer,
    // {filter: includeAction()},
    {limit: 5}
  ),
  designs: DesignsReducer,
  userImageList: UserImageReducer,
  auth: AuthReducer,
  loadingReducer: LoadingReducer,
  flickerSearch: FlickrReducer,
  selectedElement,
  userShapeList,
  templates: TemplateReducer,
  message: MessageReducer,
});

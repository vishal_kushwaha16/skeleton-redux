import {flickrSearchedImagesInitialState} from './initialState';
import {FLICKR_SEARCH_SUCCESSFUL, CLEAR_REDUX_STATE} from '../actionTypes';

export default (state = flickrSearchedImagesInitialState, action) => {
  switch (action.type) {
    case FLICKR_SEARCH_SUCCESSFUL:
      return [...action.payload];
    case CLEAR_REDUX_STATE: {
      return [...flickrSearchedImagesInitialState];
    }
    default:
      return state;
  }
};

import {authInitialState} from './initialState';
import {LOGIN_SUCCESS, LOGIN_FAILURE, LOGOUT} from '../actionTypes';

export default (state = authInitialState, action) => {
  switch (action.type) {
    case LOGIN_SUCCESS: {
      const {token, user} = action.payload;
      return {...state, isLoggedIn: true, token, user};
    }
    case LOGIN_FAILURE: {
      return {...state, isLoggedIn: false};
    }
    case LOGOUT: {
      return {
        ...state,
        authInitialState,
      };
    }
    default: {
      return state;
    }
  }
};

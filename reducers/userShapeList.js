import {userShapeInitialState} from './initialState';
import {ARTBOARD_SHAPE_UPLOAD_SUCCESS, GET_USER_SHAPE_SUCCESS} from '../actionTypes';

export default function(state = userShapeInitialState, action) {
  switch (action.type) {
    case ARTBOARD_SHAPE_UPLOAD_SUCCESS:
      return {
        ...state,
        shapes: [...state.shapes, action.payload],
      };
    case GET_USER_SHAPE_SUCCESS:
      return {
        ...state,
        shapes: action.payload,
      };
    default:
      return state;
  }
}

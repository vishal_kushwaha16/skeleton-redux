import {designBuilderInitialState} from './initialState';
import {
  ADD_ELEMENT,
  TOGGLE_DRAWER,
  UPDATE_ELEMENT,
  SET_ACTIVE_INDEX,
  GET_ARTBOARD_SUCCESS,
  UPDATE_DIMENSIONS,
  DELETE_ELEMENT,
  ADD_PAGE,
  DELETE_PAGE,
  DUPLICATE_PAGE,
  REQUEST_ARTBOARD_UPDATE,
  UPDATE_ARTBOARD_SUCCESS,
  CLEAR_REDUX_STATE,
  ADD_ARTBOARD_BACKGROUND,
  REMOVE_ARTBOARD_BACKGROUND,
  REORDER_PAGE,
  REORDER_ELEMENTS,
  SELECT_TEMPLATE,
  RENAME_ARTBOARD_NAME,
} from '../actionTypes';
// import generateUuid from '../helpers/generateUuid'

export default (state = designBuilderInitialState, action) => {
  switch (action.type) {
    case TOGGLE_DRAWER: {
      return {...state, isDrawerOpen: action.payload.isDrawerOpen};
    }

    case ADD_ELEMENT: {
      return {
        ...state,
        designData: {...action.payload.designData},
        elements: {...action.payload.elements},
      };
    }
    case DELETE_ELEMENT: {
      return {
        ...state,
        designData: {...state.designData, ...action.payload.designData},
        elements: {...action.payload.elements},
      };
    }

    case UPDATE_ELEMENT: {
      return {
        ...state,
        elements: {...state.elements, [action.payload.elementKey]: action.payload.artToChange},
      };
    }

    case SET_ACTIVE_INDEX: {
      return {...state, activeIndex: action.payload.activeIndex};
    }

    case GET_ARTBOARD_SUCCESS: {
      return {
        ...state,
        pages: [...action.payload.pages],
        designData: {...action.payload.designData},
        elements: {...action.payload.elements},
        name: action.payload.name,
        boardHeight: action.payload.boardHeight,
        boardWidth: action.payload.boardWidth,
        background: action.payload.background,
        slug: action.payload._id,
      };
    }

    case UPDATE_DIMENSIONS: {
      return {
        ...state,
        boardHeight: action.payload.height,
        boardWidth: action.payload.width,
      };
    }
    case ADD_PAGE: {
      return {
        ...state,
        pages: [...action.payload.pages],
        designData: {...action.payload.designData},
      };
    }
    case DELETE_PAGE: {
      return {
        ...state,
        pages: [...action.payload.pages],
        designData: {...action.payload.designData},
        elements: {...action.payload.elements},
      };
    }
    case DUPLICATE_PAGE: {
      return {
        ...state,
        pages: [...action.payload.pages],
        designData: {...action.payload.designData},
        elements: {...action.payload.elements},
      };
    }
    case REQUEST_ARTBOARD_UPDATE: {
      return {
        ...state,
        isLoading: true,
        isSuccess: false,
      };
    }
    case UPDATE_ARTBOARD_SUCCESS: {
      return {
        ...state,
        isLoading: false,
        isSuccess: true,
        designData: {...state.designData},
        pages: [...state.pages],
        elements: {...state.elements},
      };
    }
    case CLEAR_REDUX_STATE: {
      return {...state, ...designBuilderInitialState, past: []};
    }
    case ADD_ARTBOARD_BACKGROUND: {
      return {
        ...state,
        background: action.payload.styles,
      };
    }
    case REMOVE_ARTBOARD_BACKGROUND: {
      return {
        ...state,
        background: 'transparent',
      };
    }
    case REORDER_PAGE:
      return {
        ...state,
        pages: [...action.payload.pages],
      };
    case SELECT_TEMPLATE:
      return {
        ...state,
        ...action.payload.template,
      };

    case REORDER_ELEMENTS:
      return {
        ...state,
        designData: {
          ...state.designData,
          [action.payload.page]: action.payload.elementsOrder,
        },
      };
    case RENAME_ARTBOARD_NAME:
      return {
        ...state,
        name: action.payload,
      };
    default:
      return state;
  }
};

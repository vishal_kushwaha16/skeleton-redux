import {designsInitialState, errorInitialState} from './initialState';
import {GET_DESIGN, GET_DESIGN_LIST_SUCCESS, API_FAIL, CLEAR_REDUX_STATE} from '../actionTypes';

export default (state = designsInitialState, action) => {
  switch (action.type) {
    case GET_DESIGN:
      return {
        ...state,
        isRequested: true,
      };
    case GET_DESIGN_LIST_SUCCESS:
      return {
        ...state,
        designs: action.payload,
      };

    case API_FAIL: {
      const {statusCode, statusText, error} = action.payload;
      return {
        ...errorInitialState,
        isRequested: !state.isRequested,
        error,
        statusCode,
        statusText,
      };
    }
    case CLEAR_REDUX_STATE: {
      return {...state, ...designsInitialState};
    }

    default:
      return state;
  }
};

import {templateDataInitialState} from './initialState';
import {GET_TEMPLATES_SUCCESS} from '../actionTypes';

export default (state = templateDataInitialState, action) => {
  switch (action.type) {
    case GET_TEMPLATES_SUCCESS:
      return {
        ...state,
        allTemplates: action.payload,
      };
    default:
      return state;
  }
};

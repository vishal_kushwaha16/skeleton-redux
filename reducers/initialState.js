export const designsInitialState = {
  designs: [],
};

export const errorInitialState = {
  error: false,
  stateCode: '',
  statusText: '',
};
export const userImagesInitialState = {
  images: [],
};

export const userShapeInitialState = {
  shapes: [],
};

export const authInitialState = {
  isLoggedIn: false,
  token: '',
};

export const templateDataInitialState = [];

export const selectedElementInitialState = {
  width: 0,
  height: 0,
  positionX: 0,
  positionY: 0,
  rotateAngle: 0,
  editable: '',
  selectedPage: 0,
  showResizeValue: false,
  showRotateValue: false,
  showGridLine: false,
  type: '',
  path: [],
  styles: {},
  visible: false,
  zoomValue: 1,
  flipAxis: [],
  cursorType: '',
};

export const flickrSearchedImagesInitialState = [];

export const designBuilderInitialState = {
  // isDrawerOpen: false,
  // activeIndex: 0,
  slug: '',
  name: 'Untitled',
  boardHeight: 400,
  boardWidth: 400,
  background: 'transparent',
  elements: {
    'A841769E-A701-782C-782E-4BDA34669346': {
      key: 'A841769E-A701-782C-782E-4BDA34669346',
      type: 'text',
      content: 'text 2',
      positionX: 0,
      positionY: 0,
      width: 100,
      height: 100,
      rotateAngle: 0,
      order: 1,
      styles: {fontWeight: 'bold'},
    },
    'A841769E-A701-782C-782E-4BDA34669378': {
      key: 'A841769E-A701-782C-782E-4BDA34669378',
      type: 'text',
      content: 'text big',
      positionX: 0,
      positionY: 300,
      width: 100,
      height: 100,
      rotateAngle: 0,
      order: 1,
      styles: {fontSize: 20},
    },
    '8CD4536A-02E8-A135-1FD9-A89A80428F69': {
      key: '8CD4536A-02E8-A135-1FD9-A89A80428F69',
      type: 'image',
      url:
        'http://res.cloudinary.com/del7capiy/image/upload/v1485519792/plan/trek-to-chandrakhani_l35xpe.jpg',
      positionX: 0,
      positionY: 100,
      width: 100,
      height: 100,
      rotateAngle: 0,
      flipAxis: [1, 1],
      order: 2,
      styles: {opacity: 1},
    },
  },
  designData: {
    'A841769E-A701-782C-782E-4BDA34669332': [
      'A841769E-A701-782C-782E-4BDA34669346',
      'A841769E-A701-782C-782E-4BDA34669378',
      '8CD4536A-02E8-A135-1FD9-A89A80428F69',
    ],
  },
  pages: ['A841769E-A701-782C-782E-4BDA34669332'],

  // designData: [
  //   [
  //     {
  //       key: 'A841769E-A701-782C-782E-4BDA34669346',
  //       type: 'text',
  //       content: 'text 2',
  //       positionX: 0,
  //       positionY: 0,
  //       width: 100,
  //       height: 100,
  //       rotateAngle: 0,
  //       order: 1,
  //       styles: {fontWeight: 'bold'},
  //     },
  //     {
  //       key: 'A841769E-A701-782C-782E-4BDA34669378',
  //       type: 'text',
  //       content: 'text big',
  //       positionX: 0,
  //       positionY: 300,
  //       width: 100,
  //       height: 100,
  //       rotateAngle: 0,
  //       order: 1,
  //       styles: {fontSize: '20px'},
  //     },
  //     {
  //       key: '8CD4536A-02E8-A135-1FD9-A89A80428F69',
  //       type: 'image',
  //       url:
  //         'http://res.cloudinary.com/del7capiy/image/upload/v1485519792/plan/trek-to-chandrakhani_l35xpe.jpg',
  //       positionX: 0,
  //       positionY: 100,
  //       width: 100,
  //       height: 100,
  //       rotateAngle: 0,
  //       flipAxis: [1, 1],
  //       order: 2,
  //       styles: {opacity: 1},
  //     },
  //   ],
  // ],
};

import {userImagesInitialState} from './initialState';
import {ARTBOARD_IMAGE_UPLOAD_SUCCESS, UPDATE_USER_UPLOADED_IMAGE} from '../actionTypes';

export default function(state = userImagesInitialState, action) {
  switch (action.type) {
    case UPDATE_USER_UPLOADED_IMAGE:
      return {
        ...state,
        images: action.payload,
      };
    case ARTBOARD_IMAGE_UPLOAD_SUCCESS:
      return {
        ...state,
        images: [...state.images, {...action.payload}],
      };
    default:
      return state;
  }
}

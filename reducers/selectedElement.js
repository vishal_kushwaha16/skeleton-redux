import {selectedElementInitialState} from './initialState';
import {CURSOR_TYPE, CURSOR_POSITION} from '../actionTypes';

export default (state = selectedElementInitialState, action) => {
  switch (action.type) {
    case CURSOR_TYPE:
      return {
        ...state,
        cursorType: action.payload.cursorType,
      };
    case CURSOR_POSITION:
      return {
        ...state,
        ...action.payload.elementPosition,
      };
    default:
      return state;
  }
};

import {showSuccessToast, showFailToast} from '../../utils/Library';

export default (state = {}, action) => {
  const {type, payload} = action;

  const matches = /(.*)_(REQUEST|FAILURE|SUCCESS)/.exec(type);

  // not a *_REQUEST / *_FAIL / *_SUCCESS actions, so we ignore them
  if (!matches) return state;

  const [, requestName, requestState] = matches;

  if ((payload && payload.message) || requestState === 'CLEAR_TOAST') {
    if (requestState === 'FAILURE') {
      showFailToast(payload.message || '');
    } else {
      showSuccessToast(payload.message || '');
    }
    return {
      ...state,
      // Store errorMessage
      // e.g. stores errorMessage when receiving GET_TODOS_FAILURE
      //      else clear errorMessage when receiving GET_TODOS_REQUEST
      [requestName]:
        requestState === 'FAILURE' || requestState === 'SUCCESS' ? payload.message : '',
      message: requestState === 'FAILURE' || requestState === 'SUCCESS' ? payload.message : '',
    };
  }

  return {
    ...state,
  };
};

import axios from 'axios';
import {
  LOGIN_SUCCESS_MESSAGE,
  ERROR_MESSAGE,
  FORGOT_PASSWORD_SUCCESS,
  INVALID_USER,
} from '../utils/messages';
import {
  LOGIN_SUCCESS,
  LOGIN_FAILURE,
  API_FAIL,
  UPDATE_USER_UPLOADED_IMAGE,
  LOGOUT,
} from '../actionTypes';
import API_BASE_URL from '../apis';
import configureStore from '../../store/configureStore';
import {navigate} from '../../utils/Library';
import navigation from '../../constants/url';
// const authenticationExpirationTimings = 720; // 12 hours
const {store} = configureStore(); 
export function login(email, password) {
  return dispatch => {
    return axios
      .post(`${API_BASE_URL}/user/login`, {
        email,
        password,
      })
      .then(res => {
        if (res.data.message === 'Success') {
          const {token, user} = res.data.data;
          dispatch({
            type: LOGIN_SUCCESS,
            payload: {token, user, message: LOGIN_SUCCESS_MESSAGE},
          });
          dispatch({type: UPDATE_USER_UPLOADED_IMAGE, payload: user.images});
          navigate({routeName: navigation.Artboard});
        } else {
          dispatch({type: LOGIN_FAILURE, payload: res.data.error, message: ERROR_MESSAGE});
        }
      })
      .catch(err => {
        dispatch({type: API_FAIL, payload: {err, message: ERROR_MESSAGE}});
      });
  };
}

export function forgotPassword(email) {
  return dispatch => {
    return axios
      .post(`${API_BASE_URL}/user/forgotpassword`, {
        email,
      })
      .then(res => {
        if (res.message === 'User not Found') {
          dispatch({type: '', payload: {message: INVALID_USER}});
        } else {
          dispatch({type: '', payload: {message: FORGOT_PASSWORD_SUCCESS}});
        }
      })
      .catch(err => {
        dispatch({type: '', payload: {message: ERROR_MESSAGE}});
      });
  };
}

export function logout() {
  return dispatch => {
    dispatch({type: LOGOUT});
  };
}

export function isLoggedInUser() {
  return store.getState().auth.isLoggedIn;
}
export function authHeader() {
  const authToken = store.getState().auth.token;
  return `Bearer ${authToken}`;
}

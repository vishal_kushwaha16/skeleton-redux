import axios from 'axios';
import axiosInstance from '../utils/axiosInstance';
import STYLES_TO_REPLACE from '../utils/styleMaps';
import {
  RESIZE,
  ROTATE,
  STYLE,
  POSITION,
  DRAG,
  TEXT,
  PATH_COLOR,
  FLIP,
} from '../utils/editorActions';

import {
  ADD_ELEMENT,
  TOGGLE_DRAWER,
  UPDATE_ELEMENT,
  UPDATE_DIMENSIONS,
  SET_ACTIVE_INDEX,
  GET_DESIGN,
  GET_DESIGN_SUCCESS,
  API_FAIL,
  FLICKR_SEARCH_SUCCESSFUL,
  GET_ARTBOARD_SUCCESS,
  UPDATE_ARTBOARD_SUCCESS,
  DELETE_ELEMENT,
  ADD_PAGE,
  DELETE_PAGE,
  DUPLICATE_PAGE,
  REQUEST_ARTBOARD_UPDATE,
  CLEAR_REDUX_STATE,
  ADD_ARTBOARD_BACKGROUND,
  REMOVE_ARTBOARD_BACKGROUND,
  REORDER_PAGE,
  REORDER_ELEMENTS,
  RENAME_ARTBOARD_NAME,
} from '../actionTypes';

import WEB_URLS from '../utils/webUrls';
import API_BASE_URL from '../apis';
import generateUuid from '../utils/generateUuid';

export function reOrderPage(pages) {
  return {type: REORDER_PAGE, payload: {pages}};
}

export function addPage(pages, designData) {
  const newPageKey = generateUuid();
  pages.push(newPageKey);
  return {
    type: ADD_PAGE,
    payload: {pages, designData: {...designData, [newPageKey]: []}},
  };
}
export function deletePage(pages, designData, elements) {
  return {type: DELETE_PAGE, payload: {pages, designData, elements}};
}
export function duplicatePage(pages, designData, elements, pageHash) {
  const newPageKey = generateUuid();
  const newElements = elements;
  const newElementKeys = [];
  pages.push(newPageKey);
  Object.assign(designData, {[newPageKey]: []});

  designData[pageHash].forEach(element => {
    const key = generateUuid();
    newElementKeys.push(key);
    designData[newPageKey].push(key);
    newElements[key] = {...newElements[element], key};
  });

  return {
    type: DUPLICATE_PAGE,
    payload: {
      pages,
      designData: {...designData, [newPageKey]: newElementKeys},
      elements: newElements,
    },
  };
}
export function addElement(designData, elements) {
  return {type: ADD_ELEMENT, payload: {designData, elements}};
}
export function deleteElement(designData, elements) {
  return {type: DELETE_ELEMENT, payload: {designData, elements}};
}

export function updateElement(type, elementData, elementKey, element) {
  const artToChange = element;
  switch (type) {
    case TEXT:
      artToChange.content = elementData.content;
      break;
    case ROTATE:
      {
        const {rotateAngle} = elementData;
        artToChange.rotateAngle = rotateAngle;
      }
      break;
    case DRAG:
      {
        const {positionX, positionY, deltaX, deltaY} = elementData;
        artToChange.positionX = positionX + deltaX;
        artToChange.positionY = positionY + deltaY;
      }
      break;
    case POSITION:
      {
        const {newPosition} = elementData;
        artToChange.positionX = newPosition.left;
        artToChange.positionY = newPosition.top;
      }
      break;
    case PATH_COLOR:
      {
        console.log(artToChange);
        const {newPathColor} = elementData;
        artToChange.path = newPathColor;
      }
      break;
    case RESIZE:
      {
        const {newSize} = elementData;
        artToChange.positionX = newSize.left;
        artToChange.positionY = newSize.top;
        artToChange.width = newSize.width;
        artToChange.height = newSize.height;
      }
      break;
    case STYLE:
      {
        const {style, styleKey} = elementData;
        if (styleKey in artToChange.styles) {
          delete artToChange.styles[styleKey];
          if (STYLES_TO_REPLACE.indexOf(styleKey) > -1) {
            artToChange.styles = {...artToChange.styles, ...style};
          }
        } else {
          artToChange.styles = {...artToChange.styles, ...style};
        }
      }
      break;
    case FLIP:
      {
        const {flipAxis} = elementData;
        artToChange.flipAxis = flipAxis;
      }
      break;
    default:
      break;
  }
  return {type: UPDATE_ELEMENT, payload: {artToChange, elementKey}};
}

export function toggleDrawer(isDrawerOpen) {
  return {type: TOGGLE_DRAWER, payload: {isDrawerOpen}};
}

export function setActiveIndex(activeIndex) {
  return {type: SET_ACTIVE_INDEX, payload: {activeIndex}};
}

export function updateDimensions(height, width) {
  return {type: UPDATE_DIMENSIONS, payload: {height, width}};
}

export function getDesign(slug) {
  return dispatch => {
    dispatch({type: GET_DESIGN});

    const apiURL = `${API_BASE_URL}/design/${slug}`;
    return axios
      .get(apiURL)
      .then(response => {
        dispatch({
          type: GET_DESIGN_SUCCESS,
          payload: response.data.data.design,
        });
      })
      .catch(error => {
        dispatch({
          type: API_FAIL,
          payload: {
            error: true,
            statusCode: error.response.status,
            statusText: error.response.statusText,
          },
        });
      });
  };
}

export function searchFlickr(query, cbfailure) {
  return dispatch => {
    const APIUrl = WEB_URLS.buildUrl(
      'FLICKR_IMAGE_SEARCH_URL',
      {},
      {
        api_key: `88bc62e97be42185d2b0f38da8e78139`,
        method: 'flickr.photos.search',
        text: query,
        per_page: 10,
        format: 'json',
        nojsoncallback: 1,
      }
    );
    return axios
      .get(APIUrl)
      .then(res => {
        dispatch({
          type: FLICKR_SEARCH_SUCCESSFUL,
          payload: res.data.photos.photo.map(
            data =>
              `https://farm${data.farm}.staticflickr.com/${data.server}/${data.id}_${data.secret}_b.jpg`
          ),
        });
      })
      .catch(err => {
        dispatch({type: API_FAIL, payload: err});
        cbfailure(err);
      });
  };
}

export function getArtboard(slug) {
  return dispatch => {
    const apiURL = `${API_BASE_URL}/artboard/${slug}`;
    return axiosInstance
      .get(apiURL)
      .then(res => {
        dispatch({type: GET_ARTBOARD_SUCCESS, payload: res.data.data});
      })
      .catch(err => {
        dispatch({type: API_FAIL, payload: err});
      });
  };
}

export function requestArtboardUpdate() {
  return {type: REQUEST_ARTBOARD_UPDATE};
}
export function updateArtboard(slug, payload) {
  return dispatch => {
    const apiURL = `${API_BASE_URL}/artboard/${slug}`;
    return axiosInstance
      .put(apiURL, payload)
      .then(res => {
        dispatch({type: UPDATE_ARTBOARD_SUCCESS});
      })
      .catch(err => {
        dispatch({type: API_FAIL, payload: err});
      });
  };
}

export function clearReduxState() {
  return {type: CLEAR_REDUX_STATE};
}
export function elementReorder(page, elementsOrder) {
  return {
    type: REORDER_ELEMENTS,
    payload: {page, elementsOrder},
  };
}

export function renameArtBoard(newName) {
  return {
    type: RENAME_ARTBOARD_NAME,
    payload: newName,
  };
}

export function addArtBoardBackground(styles) {
  return {type: ADD_ARTBOARD_BACKGROUND, payload: {styles}};
}

export function removeArtboardBackround() {
  return {type: REMOVE_ARTBOARD_BACKGROUND};
}

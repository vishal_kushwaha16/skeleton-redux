import axiosInstance from '../utils/axiosInstance';
import API_BASE_URL from '../apis';
import {CREATED_NEW_ARTBOARD, ERROR_MESSAGE} from '../../constants/messages';
import {navigate} from '../../utils/Library';
import {GET_DESIGN_LIST_SUCCESS, NEW_ARTBOARD_SUCCESS, API_FAIL} from '../actionTypes';
import navigation from '../../constants/url';

export function getDesignList() {
  return dispatch => {
    return axiosInstance
      .get(`${API_BASE_URL}/artboard`)
      .then(res => {
        dispatch({type: GET_DESIGN_LIST_SUCCESS, payload: res.data.data.artboards});
      })
      .catch(err => {
        dispatch({type: API_FAIL, payload: err});
      });
  };
}

export function newArtboard(payload) {
  return dispatch => {
    const apiURL = `${API_BASE_URL}/artboard/`;
    return axiosInstance
      .post(apiURL, payload)
      .then(res => {
        // I'm not sure if we need to handle this type of action

        dispatch({type: NEW_ARTBOARD_SUCCESS, payload: {message: CREATED_NEW_ARTBOARD}});
        navigate({routeName: navigation.ARTBOARD, param: {slug: res.data.data.artboard._id}});
      })
      .catch(err => {
        dispatch({type: API_FAIL, payload: {message: ERROR_MESSAGE}});
      });
  };
}

export function inviteFriend(name, email, cbsuccess, cberror) {
  return dispatch => {
    const apiURL = `${API_BASE_URL}/user`;
    return axiosInstance
      .post(apiURL, {name, email})
      .then(res => {
        cbsuccess(res.data);
      })
      .catch(err => {
        cberror(err);
      });
  };
}

import axios from 'axios';
import {UPDATE_USER_UPLOADED_IMAGE} from '../actionTypes';
import API_BASE_URL from '../apis';

export function updateUser(userId, params) {
  return dispatch => {
    return axios
      .patch(`${API_BASE_URL}/user/${userId}`, params)
      .then(res => {
        if (res.data.message === 'Success') {
          const {user} = res.data.data;
          dispatch({type: UPDATE_USER_UPLOADED_IMAGE, payload: user.images});
        }
      })
      .catch(err => {
        console.log(err);
      });
  };
}

export function getUser(userId) {
  return dispatch => {
    return axios
      .get(`${API_BASE_URL}/user/userId`)
      .then(res => {
        if (res.data.message === 'Success') {
          console.log(res.data);
        }
      })
      .catch(err => {
        console.log(err);
      });
  };
}

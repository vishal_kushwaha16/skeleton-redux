import {
  addElement,
  toggleDrawer,
  updateElement,
  setActiveIndex,
  searchFlickr,
  updateDimensions,
  getArtboard,
  updateArtboard,
  deleteElement,
  addPage,
  deletePage,
  duplicatePage,
  requestArtboardUpdate,
  clearReduxState,
  addArtBoardBackground,
  removeArtboardBackround,
  reOrderPage,
  elementReorder,
  renameArtBoard,
} from './designBuilder';

import {uploadImage, artBoardImageUploadSuccess} from './imageUpload';
import {login, forgotPassword} from './auth';
import register from './register';
import {updateUser} from './user';
import {getDesignList, newArtboard, inviteFriend} from './listArtboard';
import {selectedElementCursor, selectedElementPosition} from './selectedElement';
import {artBoardShapeUploadSuccess, uploadShape, getUserShapes} from './shapeUpload';
import {selectTemplate, shareTemplate, getAllTemplates} from './templateData';

export {
  addElement,
  deleteElement,
  toggleDrawer,
  updateElement,
  setActiveIndex,
  uploadImage,
  artBoardImageUploadSuccess,
  getArtboard,
  updateDimensions,
  updateArtboard,
  getDesignList,
  newArtboard,
  login,
  searchFlickr,
  updateUser,
  addPage,
  deletePage,
  duplicatePage,
  inviteFriend,
  register,
  requestArtboardUpdate,
  clearReduxState,
  addArtBoardBackground,
  removeArtboardBackround,
  forgotPassword,
  reOrderPage,
  selectedElementCursor,
  selectedElementPosition,
  artBoardShapeUploadSuccess,
  uploadShape,
  elementReorder,
  getUserShapes,
  selectTemplate,
  renameArtBoard,
  shareTemplate,
  getAllTemplates,
};

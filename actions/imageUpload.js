import axios from 'axios';
import API_BASE_URL from '../apis';
import {ARTBOARD_IMAGE_UPLOAD_SUCCESS} from '../actionTypes';

export function artBoardImageUploadSuccess(payload) {
  console.log(payload);
  return {type: ARTBOARD_IMAGE_UPLOAD_SUCCESS, payload};
}

export function uploadImage(file) {
  return dispatch => {
    const apiURL = `${API_BASE_URL}/file/upload`;
    const formData = new FormData();
    formData.append('file', file.file);
    return axios
      .post(apiURL, formData)
      .then(res => {
        dispatch(artBoardImageUploadSuccess(res.data.data));
      })
      .catch(e => {
        console.error('Error Occured');
      });
  };
}

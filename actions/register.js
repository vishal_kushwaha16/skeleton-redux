import axios from 'axios';
import {API_FAIL} from '../actionTypes';
import API_BASE_URL from '../apis';

export default function register(userId, name, password, cb, cberror) {
  console.log(userId);
  return dispatch => {
    return axios
      .patch(`${API_BASE_URL}/user/${userId}`, {
        name,
        password,
      })
      .then(res => {
        console.log(res.data.data);
        if (res.data.message === 200) {
          cb();
        } else {
          cberror();
        }
      })
      .catch(err => {
        dispatch({type: API_FAIL, payload: err});
      });
  };
}

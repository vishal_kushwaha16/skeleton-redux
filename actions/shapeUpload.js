import axios from 'axios';
import API_BASE_URL from '../apis';
import {ARTBOARD_SHAPE_UPLOAD_SUCCESS, API_FAIL, GET_USER_SHAPE_SUCCESS} from '../actionTypes';
import {INVALID_SHAPE, UPLOAD_SHAPE, ERROR_MESSAGE} from '../utils/messages';

const {parse} = require('svgson');

export function uploadShape(userFile, id) {
  return dispatch => {
    const {file} = userFile;
    const apiURL = `${API_BASE_URL}/shape`;
    let uploadShapeString = '';
    const shapeName = file.name.split('.');
    if (shapeName[1] !== 'svg') {
      dispatch({type: '', payload: {message: INVALID_SHAPE}});

      return false;
    }
    const reader = new FileReader();
    reader.readAsText(file, 'UTF-8');
    reader.onload = ({target}) => {
      uploadShapeString = `${target.result}`;
      parse(uploadShapeString, {
        compat: true,
      })
        .then(parsed => {
          return axios
            .post(apiURL, {
              name: shapeName[0],
              shape: parsed,
              createdBy: id,
            })
            .then(res => {
              dispatch(artBoardShapeUploadSuccess(res.data.data));
              dispatch({type: '', payload: {message: UPLOAD_SHAPE}});
            })
            .catch(e => {
              dispatch({type: '', payload: {message: ERROR_MESSAGE}});
            });
        })
        .catch(e => {
          dispatch({type: '', payload: {message: ERROR_MESSAGE}});
        });
    };
  };
}
export function getUserShapes(userid) {
  return dispatch => {
    return axios
      .get(`${API_BASE_URL}/shape/${userid}`)
      .then(res => {
        dispatch({type: GET_USER_SHAPE_SUCCESS, payload: res.data.data.shapeCollectionResponse});
      })
      .catch(err => {
        dispatch({type: API_FAIL, payload: err});
      });
  };
}
export function artBoardShapeUploadSuccess(payload) {
  return {type: ARTBOARD_SHAPE_UPLOAD_SUCCESS, payload};
}

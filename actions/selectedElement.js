import {CURSOR_POSITION, CURSOR_TYPE} from '../actionTypes';

export function selectedElementCursor(cursorType) {
  return {type: CURSOR_TYPE, payload: {cursorType}};
}

export function selectedElementPosition(elementPosition) {
  return {type: CURSOR_POSITION, payload: {elementPosition}};
}

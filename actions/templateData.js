import axiosInstance from '../utils/axiosInstance';
import {TEMPLATE_SENT, ERROR_MESSAGE} from '../../constants/messages';

import {
  API_FAIL,
  SELECT_TEMPLATE,
  GET_TEMPLATES_REQUEST,
  GET_TEMPLATES_SUCCESS,
  SHARE_TEMPLATE_REQUEST,
  SHARE_TEMPLATE_SUCCESS,
} from '../actionTypes';

import API_BASE_URL from '../apis';

export function selectTemplate(template) {
  return {type: SELECT_TEMPLATE, payload: {template}};
}

export function getAllTemplates() {
  return dispatch => {
    dispatch({type: GET_TEMPLATES_REQUEST, payload: {message: ''}});
    const apiURL = `${API_BASE_URL}/template`;
    return axiosInstance
      .get(apiURL)
      .then(res => {
        dispatch({type: GET_TEMPLATES_SUCCESS, payload: res.data.data.templateRes});
      })
      .catch(err => {
        dispatch({type: API_FAIL, payload: err});
      });
  };
}

export function shareTemplate(
  ownerId,
  shareTemplateData,
  templateAccessibility,
  TemplateShareWith
) {
  return dispatch => {
    dispatch({type: SHARE_TEMPLATE_REQUEST});
    const apiURL = `${API_BASE_URL}/template`;
    return axiosInstance
      .post(apiURL, {
        templateOwnId: ownerId,
        templateData: shareTemplateData,
        accessibility: templateAccessibility,
        shareWith: TemplateShareWith,
      })
      .then(res => {
        dispatch({type: SHARE_TEMPLATE_SUCCESS, payload: {res, message: TEMPLATE_SENT}});
        getAllTemplates();
      })
      .catch(err => {
        dispatch({type: API_FAIL, payload: {err, message: ERROR_MESSAGE}});
      });
  };
}

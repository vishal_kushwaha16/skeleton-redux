const STYLES_TO_REPLACE = ['color', 'fontSize', 'fontFamily', 'opacity', 'letterSpacing', 'filter'];

export default STYLES_TO_REPLACE;

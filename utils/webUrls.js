import uritemplate from 'uritemplate';

const makeQueryParam = (queryParamMap = {}) => {
  let string = '?';
  let paramList;
  let index;
  if (queryParamMap) {
    Object.keys(queryParamMap).forEach(key => {
      if (queryParamMap[key].constructor === Array) {
        paramList = queryParamMap[key];
        index = paramList.length;
        while (index) {
          index -= 1;
          string += `${key}[]=${paramList[index]}&`;
        }
      } else {
        string += `${key}=${queryParamMap[key]}&`;
      }
    });
  }
  return string;
};
const WEB_URLS = {
  FLICKR_IMAGE_SEARCH_URL: 'https://www.flickr.com/services/rest/',
  IMAGE_FILTER: 'https://static.canva.com/web/images/{image}',

  buildUrl(urlName, params, queryParam) {
    return uritemplate.parse(this[urlName]).expand(params) + makeQueryParam(queryParam);
  },
};
export default WEB_URLS;

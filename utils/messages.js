export const CREATED_NEW_ARTBOARD = 'New Artboard Created';
export const ERROR_MESSAGE = 'Something Went Wrong';
export const ARTBOARD_NAME_REQUIRED = 'Artboard Name Required';
export const LOGIN_SUCCESS_MESSAGE = 'LogIn Successful';
export const INVALID_USER = 'User Not Found';
export const FORGOT_PASSWORD_SUCCESS = 'Email Sent Successfully';
export const INAVALID_USER_EMAIL_PASSWORD = 'Invaild email and password';
export const EMPTY_FIELDS_ERROR = 'Name and Email Fields are Required';
export const INVITATION_SENT = 'Invitation Sent Successfully';
export const INVALID_SHAPE = 'File Not Supported';
export const UPLOAD_SHAPE = 'Upload Shape';

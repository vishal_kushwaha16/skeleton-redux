import axios from 'axios';
import {authHeader} from '../actions/auth';

const axiosInstance = axios.create({
  headers: {
    'Content-Type': 'application/json',
    Accept: 'application/json',
  },
});

axiosInstance.interceptors.request.use(
  config => {
    const newConfig = {...config};
    newConfig.headers.Authorization = authHeader();
    return newConfig;
  },
  error => {
    Promise.reject(error);
  }
);
export default axiosInstance;
